Jediný mně známý problém je chybějící validace délky některých údajů z formuláře
(lze vyřešit ideálně na front-endu jednoduchými podmínkami testujícími délku daných řetězců).  
Dále by bylo vhodné doplnit aplikaci dalšími testy a využít GitLab pipeline
pro automatické spuštění testů při případných budoucích merge requestech.
Při rozšiřování aplikace bych jistě začal používat i vlastní Spring profily,
ale pro momentální účely jsem si vystačil s default profilem.
Také by bylo vhodné vystavit dokumentaci k REST endpointům.

Chtěl bych zmínit, že s JavaScriptem jsem doposud žádné zkušenosti neměl
a ačkoli tuším, že můj kód využívající Vue.js není úplně v pořádku,
používání této technologie pro mě bylo úplnou novinkou. Zadání úkolu mi totiž přišlo
jako skvělá příležitost se s JavaScriptem aspoň trochu sblížit. Učil jsem se tedy
framework Vue.js narychlo zhlédnutím pár tutoriálů.

Aplikace používá Maven a lze ji sestavit příkazem `mvn clean package`.  

Vytvořil Adam Bucher pro účely přijímacího řízení firmy Lundegaard.
