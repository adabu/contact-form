package com.lundegaard.contactform.config;

import com.lundegaard.contactform.dto.mapping.RequestMapper;
import com.lundegaard.contactform.dto.mapping.RequestMapperImpl;
import com.lundegaard.contactform.dto.mapping.RequestTypeMapper;
import com.lundegaard.contactform.dto.mapping.RequestTypeMapperImpl;
import com.lundegaard.contactform.dto.mapping.RequesterMapper;
import com.lundegaard.contactform.dto.mapping.RequesterMapperImpl;
import com.lundegaard.contactform.repository.RequestRepository;
import com.lundegaard.contactform.repository.RequestTypeRepository;
import com.lundegaard.contactform.service.RequestService;
import com.lundegaard.contactform.service.RequestServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * A configuration for beans of the request logic.
 */
@Configuration
public class RequestConfig {

    @Bean
    public RequestTypeMapper requestTypeMapper() {
        return new RequestTypeMapperImpl();
    }

    @Bean
    public RequesterMapper requesterMapper() {
        return new RequesterMapperImpl();
    }

    @Bean
    public RequestMapper requestMapper(RequestTypeMapper requestTypeMapper, RequesterMapper requesterMapper) {
        return new RequestMapperImpl(requestTypeMapper, requesterMapper);
    }

    @Bean
    public RequestService requestService(RequestRepository requestRepository,
                                         RequestTypeRepository requestTypeRepository,
                                         RequestMapper requestMapper, RequestTypeMapper requestTypeMapper) {
        return new RequestServiceImpl(requestRepository, requestTypeRepository, requestMapper, requestTypeMapper);
    }

}
