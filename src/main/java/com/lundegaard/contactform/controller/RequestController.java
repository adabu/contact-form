package com.lundegaard.contactform.controller;

import com.lundegaard.contactform.dto.RequestDto;
import com.lundegaard.contactform.dto.RequestTypeDto;
import com.lundegaard.contactform.service.RequestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * A REST controller for controlling endpoints associated with requests.
 */
@RestController
@RequestMapping("/request")
public class RequestController {

    private final RequestService requestService;

    private static final Logger LOGGER = LoggerFactory.getLogger(RequestController.class);

    public RequestController(RequestService requestService) {
        this.requestService = requestService;
    }

    @PostMapping
    public void createRequest(@RequestBody RequestDto requestDto) {
        LOGGER.info("Create request with {} as body parameter was called.", requestDto);
        requestService.create(requestDto);
    }

    @GetMapping("/type")
    public List<RequestTypeDto> getAllTypes() {
        LOGGER.info("Get all types was called.");
        List<RequestTypeDto> types = requestService.getTypes();
        LOGGER.info("Returning {} types.", types.size());
        return types;
    }

}
