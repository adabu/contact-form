package com.lundegaard.contactform.dto;

/**
 * An abstract data transfer object with id.
 */
public abstract class AbstractDto {

    private final Long id;

    public AbstractDto(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

}
