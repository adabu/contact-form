package com.lundegaard.contactform.dto;

/**
 * Represents a request.
 * This is a data transfer object.
 */
public class RequestDto extends AbstractDto {

    private final RequestTypeDto type;
    private final String subject;
    private final String policyNumber;
    private final RequesterDto requester;
    private final String message;

    private RequestDto(Long id, RequestTypeDto type, String subject, String policyNumber,
                       RequesterDto requester, String message) {
        super(id);
        this.type = type;
        this.subject = subject;
        this.policyNumber = policyNumber;
        this.requester = requester;
        this.message = message;
    }

    public RequestTypeDto getType() {
        return type;
    }

    public String getSubject() {
        return subject;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public RequesterDto getRequester() {
        return requester;
    }

    public String getMessage() {
        return message;
    }

    public static RequestDtoBuilder builder() {
        return new RequestDtoBuilder();
    }

    public static class RequestDtoBuilder {

        private Long id;
        private RequestTypeDto type;
        private String subject;
        private String policyNumber;
        private RequesterDto requester;
        private String message;

        public RequestDtoBuilder setId(Long id) {
            this.id = id;
            return this;
        }

        public RequestDtoBuilder setType(RequestTypeDto type) {
            this.type = type;
            return this;
        }

        public RequestDtoBuilder setSubject(String subject) {
            this.subject = subject;
            return this;
        }

        public RequestDtoBuilder setPolicyNumber(String policyNumber) {
            this.policyNumber = policyNumber;
            return this;
        }

        public RequestDtoBuilder setRequester(RequesterDto requester) {
            this.requester = requester;
            return this;
        }

        public RequestDtoBuilder setMessage(String message) {
            this.message = message;
            return this;
        }

        public RequestDto build() {
            return new RequestDto(id, type, subject, policyNumber, requester, message);
        }

    }

}
