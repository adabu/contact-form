package com.lundegaard.contactform.dto;

/**
 * A type of request.
 * This is a data transfer object.
 */
public class RequestTypeDto extends AbstractDto {

    private final String name;

    public RequestTypeDto(Long id, String name) {
        super(id);
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
