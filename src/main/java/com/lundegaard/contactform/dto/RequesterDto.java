package com.lundegaard.contactform.dto;

/**
 * A person who is requesting for something.
 * This is a data transfer object.
 */
public class RequesterDto {

    private final String firstname;
    private final String lastname;

    public RequesterDto(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

}
