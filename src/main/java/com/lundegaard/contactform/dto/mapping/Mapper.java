package com.lundegaard.contactform.dto.mapping;

/**
 * Contains methods for mapping between data transfer object and entity instances.
 *
 * @param <Dto> the DTO (data transfer object) type
 * @param <Entity> the entity type
 */
public interface Mapper<Dto, Entity> {

    /**
     * Returns DTO with properties set in accordance with {@code entity}.
     *
     * @param entity the entity to map
     * @return the mapped DTO
     */
    Dto toDto(Entity entity);

    /**
     * Returns entity with properties set in accordance with {@code dto}.
     *
     * @param dto the DTO to map
     * @return the mapped entity
     */
    Entity toEntity(Dto dto);

}
