package com.lundegaard.contactform.dto.mapping;

import com.lundegaard.contactform.dto.RequestDto;
import com.lundegaard.contactform.entity.Request;

/**
 * Contains methods for mapping between instances of {@code RequestDto} and {@code Request}.
 */
public interface RequestMapper extends Mapper<RequestDto, Request> {
}
