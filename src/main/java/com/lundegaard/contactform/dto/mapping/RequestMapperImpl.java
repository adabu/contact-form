package com.lundegaard.contactform.dto.mapping;

import com.lundegaard.contactform.dto.RequestDto;
import com.lundegaard.contactform.dto.RequestTypeDto;
import com.lundegaard.contactform.dto.RequesterDto;
import com.lundegaard.contactform.entity.Request;
import com.lundegaard.contactform.entity.RequestType;
import com.lundegaard.contactform.entity.Requester;

public class RequestMapperImpl implements RequestMapper {

    private final RequestTypeMapper requestTypeMapper;
    private final RequesterMapper requesterMapper;

    public RequestMapperImpl(RequestTypeMapper requestTypeMapper, RequesterMapper requesterMapper) {
        this.requestTypeMapper = requestTypeMapper;
        this.requesterMapper = requesterMapper;
    }

    @Override
    public RequestDto toDto(Request entity) {
        RequestTypeDto typeDto = requestTypeMapper.toDto(entity.getType());
        RequesterDto requesterDto = requesterMapper.toDto(entity.getRequester());
        return RequestDto.builder()
                .setId(entity.getId())
                .setType(typeDto)
                .setSubject(entity.getSubject())
                .setPolicyNumber(entity.getPolicyNumber())
                .setRequester(requesterDto)
                .setMessage(entity.getMessage())
                .build();
    }

    @Override
    public Request toEntity(RequestDto dto) {
        Request request = new Request();

        request.setId(dto.getId());

        RequestType requestType = requestTypeMapper.toEntity(dto.getType());
        request.setType(requestType);

        request.setSubject(dto.getSubject());

        request.setPolicyNumber(dto.getPolicyNumber());

        Requester requester = requesterMapper.toEntity(dto.getRequester());
        request.setRequester(requester);

        request.setMessage(dto.getMessage());

        return request;
    }

}
