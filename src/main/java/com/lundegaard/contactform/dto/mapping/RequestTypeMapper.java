package com.lundegaard.contactform.dto.mapping;

import com.lundegaard.contactform.dto.RequestTypeDto;
import com.lundegaard.contactform.entity.RequestType;

/**
 * Contains methods for mapping between instances of {@code RequestTypeDto} and {@code RequestType}.
 */
public interface RequestTypeMapper extends Mapper<RequestTypeDto, RequestType> {
}
