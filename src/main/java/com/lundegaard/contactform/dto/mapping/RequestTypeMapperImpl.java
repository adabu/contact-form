package com.lundegaard.contactform.dto.mapping;

import com.lundegaard.contactform.dto.RequestTypeDto;
import com.lundegaard.contactform.entity.RequestType;

public class RequestTypeMapperImpl implements RequestTypeMapper {

    @Override
    public RequestTypeDto toDto(RequestType entity) {
        return new RequestTypeDto(entity.getId(), entity.getName());
    }

    @Override
    public RequestType toEntity(RequestTypeDto dto) {
        RequestType requestTypeEntity = new RequestType();

        requestTypeEntity.setId(dto.getId());
        requestTypeEntity.setName(dto.getName());

        return requestTypeEntity;
    }

}
