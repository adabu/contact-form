package com.lundegaard.contactform.dto.mapping;

import com.lundegaard.contactform.dto.RequesterDto;
import com.lundegaard.contactform.entity.Requester;

/**
 * Contains methods for mapping between instances of {@code RequesterDto} and {@code Requester}.
 */
public interface RequesterMapper extends Mapper<RequesterDto, Requester> {
}
