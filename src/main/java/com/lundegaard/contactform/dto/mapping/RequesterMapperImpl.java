package com.lundegaard.contactform.dto.mapping;

import com.lundegaard.contactform.dto.RequesterDto;
import com.lundegaard.contactform.entity.Requester;

public class RequesterMapperImpl implements RequesterMapper {

    @Override
    public RequesterDto toDto(Requester entity) {
        return new RequesterDto(entity.getFirstname(), entity.getLastname());
    }

    @Override
    public Requester toEntity(RequesterDto dto) {
        Requester requesterEntity = new Requester();

        requesterEntity.setFirstname(dto.getFirstname());
        requesterEntity.setLastname(dto.getLastname());

        return requesterEntity;
    }

}
