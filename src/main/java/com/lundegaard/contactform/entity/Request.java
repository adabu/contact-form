package com.lundegaard.contactform.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * A request entity for purposes of persisting.
 */
@Entity
@Table(name = "request")
public class Request extends AbstractEntity {

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "type_id", nullable = false, foreignKey = @ForeignKey(name = "request_request_type_fk"))
    private RequestType type;
    @Column(name = "subject", nullable = false)
    private String subject;
    @Column(name = "policy_number", nullable = false)
    private String policyNumber;
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(
                    name = "firstname",
                    column = @Column(name = "user_firstname", nullable = false, length = 50)
            ),
            @AttributeOverride(
                    name = "lastname",
                    column = @Column(name = "user_lastname", nullable = false, length = 50)
            )
    })
    private Requester requester;
    @Column(name = "message", nullable = false, length = 2000)
    private String message;

    public RequestType getType() {
        return type;
    }

    public void setType(RequestType type) {
        this.type = type;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public Requester getRequester() {
        return requester;
    }

    public void setRequester(Requester requester) {
        this.requester = requester;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
