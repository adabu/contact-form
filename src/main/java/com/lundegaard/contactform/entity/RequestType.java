package com.lundegaard.contactform.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * A type of request entity for purposes of persisting.
 */
@Entity
@Table(name = "request_type")
public class RequestType extends AbstractEntity {

    @Column(name = "name", nullable = false, length = 40)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
