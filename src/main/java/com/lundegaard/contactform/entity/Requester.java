package com.lundegaard.contactform.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * An embeddable requester entity for purposes of persisting.
 */
@Embeddable
public class Requester {

    @Column(name = "firstname", nullable = false, length = 50)
    private String firstname;
    @Column(name = "lastname", nullable = false, length = 50)
    private String lastname;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

}
