package com.lundegaard.contactform.repository;

import com.lundegaard.contactform.entity.Request;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * A JPA repository for storing and managing {@code Request} objects.
 */
public interface RequestRepository extends JpaRepository<Request, Long> {
}
