package com.lundegaard.contactform.repository;

import com.lundegaard.contactform.entity.RequestType;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * A JPA repository for storing and managing {@code RequestType} objects.
 */
public interface RequestTypeRepository extends JpaRepository<RequestType, Long> {
}
