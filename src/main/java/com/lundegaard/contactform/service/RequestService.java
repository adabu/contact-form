package com.lundegaard.contactform.service;

import com.lundegaard.contactform.dto.RequestDto;
import com.lundegaard.contactform.dto.RequestTypeDto;

import java.util.List;

/**
 * A service for working with {@code RequestDto} requests.
 */
public interface RequestService {

    /**
     * Creates new request in request repository.
     *
     * @param requestDto the request DTO to be created
     */
    void create(RequestDto requestDto);

    /**
     * Returns all available request types.
     *
     * @return a list containing available request types
     */
    List<RequestTypeDto> getTypes();

}
