package com.lundegaard.contactform.service;

import com.lundegaard.contactform.dto.RequestDto;
import com.lundegaard.contactform.dto.RequestTypeDto;
import com.lundegaard.contactform.dto.mapping.RequestMapper;
import com.lundegaard.contactform.dto.mapping.RequestTypeMapper;
import com.lundegaard.contactform.entity.Request;
import com.lundegaard.contactform.entity.RequestType;
import com.lundegaard.contactform.repository.RequestRepository;
import com.lundegaard.contactform.repository.RequestTypeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Transactional
public class RequestServiceImpl implements RequestService {

    private final RequestRepository requestRepository;
    private final RequestTypeRepository requestTypeRepository;
    private final RequestMapper requestMapper;
    private final RequestTypeMapper requestTypeMapper;

    private static final Logger LOGGER = LoggerFactory.getLogger(RequestServiceImpl.class);

    public RequestServiceImpl(RequestRepository requestRepository, RequestTypeRepository requestTypeRepository,
                              RequestMapper requestMapper, RequestTypeMapper requestTypeMapper) {
        this.requestRepository = requestRepository;
        this.requestTypeRepository = requestTypeRepository;
        this.requestMapper = requestMapper;
        this.requestTypeMapper = requestTypeMapper;
    }

    @Override
    public void create(RequestDto requestDto) {
        LOGGER.info("Create request with policy number {} was called.", requestDto.getPolicyNumber());
        Request requestEntity = requestMapper.toEntity(requestDto);
        requestRepository.save(requestEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public List<RequestTypeDto> getTypes() {
        LOGGER.info("Get types of request was called.");
        List<RequestType> types = requestTypeRepository.findAll();
        List<RequestTypeDto> requestTypeDtos = types.stream()
                .map(requestTypeMapper::toDto)
                .collect(Collectors.toList());
        LOGGER.info("Returning {} types.", requestTypeDtos.size());
        return requestTypeDtos;
    }

}
