CREATE TABLE request_type
(
    id   bigint                NOT NULL,
    name character varying(40) NOT NULL
);

CREATE SEQUENCE communication.request_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE communication.request_type_id_seq OWNED BY request_type.id;

ALTER TABLE ONLY request_type
    ALTER COLUMN id SET DEFAULT nextval('communication.request_type_id_seq'::regclass);

ALTER TABLE ONLY request_type
    ADD CONSTRAINT request_type_pkey PRIMARY KEY (id);
