CREATE TABLE request
(
    id             bigint                  NOT NULL,
    message        character varying(2000) NOT NULL,
    policy_number  character varying(255)  NOT NULL,
    user_firstname character varying(50)   NOT NULL,
    user_lastname  character varying(50)   NOT NULL,
    subject        character varying(255)  NOT NULL,
    type_id        bigint                  NOT NULL
);

CREATE SEQUENCE communication.request_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE communication.request_id_seq OWNED BY request.id;

ALTER TABLE ONLY request
    ALTER COLUMN id SET DEFAULT nextval('request_id_seq'::regclass);

ALTER TABLE ONLY request
    ADD CONSTRAINT request_pkey PRIMARY KEY (id);

ALTER TABLE ONLY request
    ADD CONSTRAINT request_request_type_fk FOREIGN KEY (type_id) REFERENCES request_type (id);
