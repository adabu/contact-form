const contactForm = new Vue({
	el: "#contact-form",
	data: {
		availableTypes: [],
		request: {
			type: {},
			subject: null,
			policyNumber: null,
			requester: {},
			message: null
		},
		messageMaxLength: 2000,
		errors: [],
		styleObject: {
			marginBottom: "20px"
		}
	},
	methods: {
		fetchAvailableTypes() {
			fetch("http://localhost:8080/request/type")
				.then(response => response.json())
				.then(data => {
					this.availableTypes = data
				});
		},
		limitMessage(request, maxCount) {
			if (request.message.length > maxCount) {
				request.message = request.message.substring(0, maxCount)
			}
		},
		clearForm(request) {
			request.type = null
			request.subject = ""
			request.policyNumber = ""
			request.requester.firstname = ""
			request.requester.lastname = ""
			request.message = ""
		},
		isAlphanumeric(string) {
			const regex = /^[a-zA-Z0-9]+$/;
			return regex.test(string)
		},
		isAlpha(string) {
			const regex = /^[a-zA-Z]+$/;
			return regex.test(string)
		},
		valuesAreValid(request) {
			if (this.isAlphanumeric(request.policyNumber) && this.isAlpha(request.requester.firstname) && this.isAlpha(request.requester.lastname) &&
					request.type && request.subject && request.requester.firstname && request.requester.lastname && request.message) {
				return true;
			}
		},
		send(request) {
			this.errors = []
			if (this.valuesAreValid(request)) {
				fetch("http://localhost:8080/request", {
					body: JSON.stringify(request),
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					}
				})
				this.clearForm(request)
			} else {
				if (!this.isAlphanumeric(request.policyNumber)) {
					this.errors.push("Policy number must contain alphanumeric characters only.")
				}
				if (!this.isAlpha(request.requester.firstname)) {
					this.errors.push("Name must contain alpha characters only.")
				}
				if (!this.isAlpha(request.requester.lastname)) {
					this.errors.push("Surname must contain alpha characters only.")
				}
				if (!request.type || !request.subject || !request.requester.firstname || !request.requester.lastname || !request.message) {
					this.errors.push("All fields must be filled.")
				}
			}
		}
	},
	mounted() {
		this.fetchAvailableTypes()
		this.clearForm(this.request)
	},
	template: `
		<div>
			<p v-if="errors.length > 0">
				Please correct the following mistakes:
				<ul>
					<li v-for="error in errors">{{error}}</li>
				</ul>
			</p>

			<div v-bind:style="styleObject">
				<label for="requestSelect">Kind of Request</label> <br />
				<select id="requestSelect" v-model="request.type">
					<option v-for="type in availableTypes" v-bind:value="type">{{type.name}}</option>
				</select>
			</div>

			<div v-bind:style="styleObject">
				<label for="subjectInput">Subject</label> <br />
				<input id="subjectInput" v-model="request.subject" />
			</div>

			<div v-bind:style="styleObject">
				<label for="policyNumberInput">Policy number</label> <br />
				<input id="policyNumberInput" v-model="request.policyNumber" />
			</div>
			
			<div v-bind:style="styleObject">
				<label for="firstnameInput">Name</label> <br />
				<input id="firstnameInput" v-model="request.requester.firstname" />
			</div>
			
			<div v-bind:style="styleObject">
				<label for="lastnameInput">Surname</label> <br />
				<input id="lastnameInput" v-model="request.requester.lastname" />
			</div>
			
			<div v-bind:style="styleObject">
				<label for="messageInput">Your request</label> <br />
				<textarea id="messageInput" v-model="request.message" v-on:keyup="limitMessage(request, messageMaxLength)" />
			</div>

			<button v-on:click="send(request)">SEND REQUEST</button>
		</div>
	`
})
