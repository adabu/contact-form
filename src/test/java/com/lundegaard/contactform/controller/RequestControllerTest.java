package com.lundegaard.contactform.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lundegaard.contactform.dto.RequestDto;
import com.lundegaard.contactform.dto.RequestTypeDto;
import com.lundegaard.contactform.service.RequestService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(RequestController.class)
class RequestControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private RequestService requestService;

    @Test
    void postRequestCreatesRequest() throws Exception {
        final long id = 1L;
        final RequestDto requestDto = RequestDto.builder().setId(id).build();

        mockMvc.perform(
                post("/request")
                        .content(objectMapper.writeValueAsString(requestDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(requestService).create(any(RequestDto.class));
    }

    @Test
    void getRequestTypeReturnsTypes() throws Exception {
        final long requestTypeId = 1L;
        final RequestTypeDto requestTypeDto = new RequestTypeDto(requestTypeId, null);
        when(requestService.getTypes()).thenReturn(List.of(requestTypeDto));

        mockMvc.perform(get("/request/type"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(requestTypeId));

        verify(requestService).getTypes();
    }

}