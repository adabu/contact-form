package com.lundegaard.contactform.dto.mapping;

import com.lundegaard.contactform.dto.RequestDto;
import com.lundegaard.contactform.dto.RequestTypeDto;
import com.lundegaard.contactform.dto.RequesterDto;
import com.lundegaard.contactform.entity.Request;
import com.lundegaard.contactform.entity.RequestType;
import com.lundegaard.contactform.entity.Requester;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

class RequestMapperImplTest {

    private RequestMapper requestMapper;
    @Mock
    private RequesterMapper requesterMapper;
    @Mock
    private RequestTypeMapper requestTypeMapper;

    @BeforeEach
    void beforeEach() {
        MockitoAnnotations.initMocks(this);

        requestMapper = new RequestMapperImpl(requestTypeMapper, requesterMapper);
    }

    @Test
    void mappingToDtoReturnsCorrectDto() {
        Request request = new Request();

        final Long id = 1L;
        request.setId(id);

        final RequestType requestType = new RequestType();
        request.setType(requestType);
        final RequestTypeDto requestTypeDto = new RequestTypeDto(null, null);
        when(requestTypeMapper.toDto(requestType)).thenReturn(requestTypeDto);

        final String subject = "Subject";
        request.setSubject(subject);

        final String policyNumber = "Policy number";
        request.setPolicyNumber(policyNumber);

        final Requester requester = new Requester();
        request.setRequester(requester);
        final RequesterDto requesterDto = new RequesterDto(null, null);
        when(requesterMapper.toDto(requester)).thenReturn(requesterDto);

        final String message = "Message";
        request.setMessage(message);

        RequestDto requestDto = requestMapper.toDto(request);

        assertThat(requestDto.getId()).isEqualTo(id);
        assertThat(requestDto.getType()).isSameAs(requestTypeDto);
        assertThat(requestDto.getSubject()).isEqualTo(subject);
        assertThat(requestDto.getPolicyNumber()).isEqualTo(policyNumber);
        assertThat(requestDto.getRequester()).isSameAs(requesterDto);
        assertThat(requestDto.getMessage()).isEqualTo(message);
    }

    @Test
    void mappingToEntityReturnsCorrectEntity() {
        final Long id = 1L;
        final RequestTypeDto requestTypeDto = new RequestTypeDto(null, null);
        final String subject = "Subject";
        final String policyNumber = "Policy number";
        final RequesterDto requesterDto = new RequesterDto(null, null);
        final String message = "Message";
        RequestDto requestDto = RequestDto.builder()
                .setId(id)
                .setType(requestTypeDto)
                .setSubject(subject)
                .setPolicyNumber(policyNumber)
                .setRequester(requesterDto)
                .setMessage(message)
                .build();

        final RequestType requestType = new RequestType();
        when(requestTypeMapper.toEntity(requestTypeDto)).thenReturn(requestType);

        final Requester requester = new Requester();
        when(requesterMapper.toEntity(requesterDto)).thenReturn(requester);

        Request request = requestMapper.toEntity(requestDto);

        assertThat(request.getId()).isEqualTo(id);
        assertThat(request.getType()).isSameAs(requestType);
        assertThat(request.getSubject()).isEqualTo(subject);
        assertThat(request.getPolicyNumber()).isEqualTo(policyNumber);
        assertThat(request.getRequester()).isSameAs(requester);
        assertThat(request.getMessage()).isEqualTo(message);
    }

}